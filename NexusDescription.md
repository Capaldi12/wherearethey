# About

Ever wondered where exactly your friends are right now? Tired of asking the same questions over and over again? Now, this mod is for you. I had same problems so I decided to throw this together quickly (or so I thought). So, here we go, I guess. Enjoy.

# How to use

Just install this mod, and you will get simple overlay right below your cash, that will display locations of players currently playing with you. If you don't like placement of overlay, you can always change it in config (Generic Mod Config Menu is advised). You can also change wether mod should display your own location.

# Localization

Thanks to shekurika, [shekurika](https://www.nexusmods.com/stardewvalley/users/69153238) (shekurika#8884 on Discord), I have all vanilla location names and some modded ones (e.g. SVE), in English, German, Japanese, Portuguese, Turkish and Chinese. I also translated vanilla location names in Russian. However, there is still a lot missing (e.g. there's no modded locations in German and Russian), as well as some languages entirely. If you have new translation or additions/improvements to existing one, feel free to message me or make a pull request on GitLab.

# Known Issues

Mod does not work properly with Local Multiplayer (SplitScreen). I will be fixing this soon

# Contact me

If you have bug reports, suggestions, translations or questions, feel free to message me on Discord (4sent4#7373) or create Issue/Pull Request in [mod's repository on GitLab](https://gitlab.com/Capaldi12/wherearethey).